const state = {  
    list: [{ name: 'Lucas Alves', email: 'lucas.alves@nodo.cc', category_id: 2 }]
}

const mutations = {
    'ADD_USER': (state, payload) => {
        state.list.push(payload)
    }
}

const actions = {
    newUser: (context, payload) => {
        context.commit('ADD_USER', payload)
    }
}

const getters = {
    users: state => state.list
}

export default { state, mutations, getters, actions }